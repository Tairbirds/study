#!/bin/bash

# Compile the C program
# gcc -o my_program main.c

# # Test each input file
# for input_file in testcases/input*.txt; do
#     test_case=$(basename "$input_file" .txt)
#     expected_output="testcases/output${test_case#input}.txt"
#     temp_output="temp_output.txt"

#     # Execute the program with the current input file
#     ./my_program < "$input_file" > "$temp_output"

#     # Compare the output with the expected output
#     if diff -q "$temp_output" "$expected_output"; then
#         echo "Test case ${test_case} passed."
#     else
#         echo "Test case ${test_case} failed."
#         exit 1
#     fi
# done

# Clean up temporary files
# rm -f my_program temp_output.txt


#!/bin/bash

# Compile the C program
gcc -o my_program main1.c

# Test each input file
for input_file in testcases/input*.txt; do
    test_case=$(basename "$input_file" .txt)
    expected_output="testcases/output${test_case#input}.txt"
    temp_output="temp_output_${test_case}.txt"  # Create a unique temp output file for each test case

    # Execute the program with the current input file and save the output to the unique temp output file
    ./my_program < "$input_file" > "$temp_output"

    # Compare the output with the expected output
    if diff -q "$temp_output" "$expected_output"; then
        echo "Test case ${test_case} passed."
    else
        echo "Test case ${test_case} failed."
        exit 1
    fi
done

# Clean up temporary files
# rm -f my_program temp_output_*.txt
