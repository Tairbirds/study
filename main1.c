#include <stdio.h>

int main() {
    int n, i;

    printf("There is space for 25 grades available.\n");
    printf("How many grades to analyze? Enter grades: \n");
    scanf("%d", &n);

    if (n <= 0) {
        printf("输入的数字个数应为正整数。\n");
        return 1; // 返回错误代码
    }

    int numbers[n];
    int sum = 0;

    // printf("请输入%d个数字：\n", n);
    for (i = 0; i < n; i++) {
        scanf("%d", &numbers[i]);
        sum += numbers[i];
    }

    int max = numbers[0];
    int min = numbers[0];

    for (i = 1; i < n; i++) {
        if (numbers[i] > max) {
            max = numbers[i];
        }
        if (numbers[i] < min) {
            min = numbers[i];
        }
    }

    double average = (double)sum / n;

    printf("Average: %.2f\n", average);
    printf("Min: %d\n", min);
    printf("Max: %d\n", max);

    return 0;
}
