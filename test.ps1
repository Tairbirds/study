# Set the path to the compiled C program (executable)
$program_path = "$env:CI_PROJECT_DIR/my_program"

# Set the working directory to the repository root
$working_directory = "$env:CI_PROJECT_DIR"

# Create a directory to store test results
$results_directory = "${working_directory}\test_results"
New-Item -ItemType Directory -Path $results_directory -Force | Out-Null

# Test each input file
foreach ($input_file in Get-ChildItem -Path "${working_directory}\testcases\input*.txt") {
    $test_case = [System.IO.Path]::GetFileNameWithoutExtension($input_file.Name)
    $expected_output = "${working_directory}\testcases\output${test_case -replace 'input',''}.txt"
    $temp_output_dir = "${results_directory}\${test_case}"
    $temp_output = "${temp_output_dir}\temp_output.txt"  # Create a unique temp output file for each test case

    # Create a directory for this test case
    New-Item -ItemType Directory -Path $temp_output_dir | Out-Null

    # Execute the program with the input file and save the output to the temp output file
    Start-Process -WorkingDirectory $working_directory -FilePath $program_path -ArgumentList "$input_file.FullName", ">$temp_output"

    # Wait for the process to finish
    Start-Sleep -Seconds 1

    # Compare the output with the expected output
    if ((Get-Content $temp_output) -eq (Get-Content $expected_output)) {
        Write-Host "Test case ${test_case} passed."
    } else {
        Write-Host "Test case ${test_case} failed."
        exit 1
    }
}

# Clean up temporary files
Remove-Item -Path "$program_path", $results_directory -Recurse

