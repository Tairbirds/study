# Set the path to the C program
$program_path = "$env:CI_PROJECT_DIR/my_program"

# Create a directory to store the test results
mkdir -force "$env:CI_PROJECT_DIR/test_results"

# Test each input file
foreach ($input_file in Get-ChildItem -Path "$env:CI_PROJECT_DIR/testcases/input*.txt") {
    $test_case = [System.IO.Path]::GetFileNameWithoutExtension($input_file.Name)
    $expected_output = "$env:CI_PROJECT_DIR/testcases/output${test_case -replace 'input',''}.txt"
    $temp_output_dir = "$env:CI_PROJECT_DIR/test_results/${test_case}"
    $temp_output = "${temp_output_dir}/temp_output.txt"  # Create a unique temp output file for each test case

    # Create a directory for this test case
    mkdir -force $temp_output_dir

    # Read the input file and pass it as an argument to the C program
    $input_data = Get-Content $input_file.FullName
    & $program_path $input_data > $temp_output

    # Save the output file
    Move-Item -Path $temp_output -Destination $temp_output -Force

    # Compare the output with the expected output
    if ((Get-Content $temp_output) -eq (Get-Content $expected_output)) {
        Write-Host "Test case ${test_case} passed."
    } else {
        Write-Host "Test case ${test_case} failed."
        exit 1
    }
}

# Clean up temporary files
Remove-Item -Path $program_path, "$env:CI_PROJECT_DIR/test_results" -Recurse